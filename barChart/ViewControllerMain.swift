//
//  ViewControllerMain.swift
//  barChart
//
//  Created by Sierra 4 on 12/04/17.
//  Copyright © 2017 codebrew. All rights reserved.
//

import UIKit

class ViewControllerMain: UIViewController {
    var flag : Bool = false
    @IBOutlet weak var viewGraph: ViewBar!
    override func viewDidLoad() {
        super.viewDidLoad()
          sendData()
    }
    func sendData(){
        viewGraph.arrayHour = DataArray.aryHour
        viewGraph.arrayMinute = DataArray.aryMinute
        viewGraph.arrayColour = DataArray.aryColour
        viewGraph.arrayTagNumber = DataArray.aryTag
    }

}
