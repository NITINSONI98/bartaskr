//
//  Array.swift
//  barChart
//
//  Created by Sierra 4 on 12/04/17.
//  Copyright © 2017 codebrew. All rights reserved.
//

import Foundation
class DataArray{

    static var aryTag : [Int] = [1,2,3,1,2]
    static var aryColour : [Int] = [0x0FB1EC,0xFFAE90,0x7AE1FC,0x0FB1EC,0xFFAE90]
    static var aryHour : [Int] = [2,6,4,2,8]
    static var aryMinute : [Int] = [0,120,0,0,0]
    
}
