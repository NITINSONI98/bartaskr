//
//  ViewBar.swift
//  barChart
//
//  Created by Sierra 4 on 12/04/17.
//  Copyright © 2017 codebrew. All rights reserved.
//

import UIKit

class ViewBar: UIView {
    var viewArray = [UIView]()
    var tagArray = [Int]()
    var flag = [Int]()
    var myView = UIView()
    var flagData : Bool = false
    var arrayHour = [Int]()
    var arrayMinute = [Int]()
    var arrayColour = [Int]()
    var arrayTagNumber = [Int]()
    
    //    var arrayHour = DataArray.aryHour
    //    var arrayMinute = DataArray.aryMinute
    //    var arrayColour = DataArray.aryColour
    //    var arraytagNumber = DataArray.aryTag
    //    var arrayCondition = DataArray.aryConditions
    //    var arrayImage = DataArray.aryImages
    //    var arrayTime = DataArray.aryTimes
    var frameView : CGFloat = 0
    
    override func draw(_ rect: CGRect) {
        data()
    }
    
    func data(){
        flagData = checkData()
        if(flagData){
            for i in 0..<arrayHour.count{
                addBar(hour : arrayHour[i], minute : arrayMinute[i], color: arrayColour[i], tagNumber : arrayTagNumber[i])
            }
        }
    }
    
    func checkData() -> Bool{
        var sumHour : Int = 0
        var sumMinute : Int = 0
        var sum : Int = 0
        if(DataArray.aryHour.count == DataArray.aryMinute.count && DataArray.aryHour.count == DataArray.aryTag.count){
            for i in 0..<DataArray.aryHour.count{
                sumHour = sumHour + DataArray.aryHour[i]
                sumMinute = sumMinute + DataArray.aryMinute[i]
            }
            sum = 60 * sumHour + sumMinute
            if(sum == 1440){
                return true
            }
            else{
                return false
            }
        }
        else{
            return false
        }
    }
    
    //Mark: - Add Bars
    
    func addBar(hour : Int , minute : Int , color : Int,tagNumber : Int){
        let screenSize: CGRect = UIScreen.main.bounds
        let screenWidth = screenSize.width
        let minuteduration = Float(screenWidth / 1440)
        var length = Float()
        length = minuteduration * Float(minute + hour * 60)
        myView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: CGFloat(length), height: self.frame.height))
        myView.frame.origin.x = frameView
        frameView = frameView + CGFloat(length)
        myView.backgroundColor = UIColor(rgb: color)
        self.addSubview(myView)
        myView.tag = tagNumber
        viewArray.append(myView)
        tagArray.append(tagNumber)
        flag.append(0)
        let gesture = UITapGestureRecognizer(target: self, action:  #selector (self.someAction (_:)))
        myView.addGestureRecognizer(gesture)
    }
    
    //Mark: - Add Shadow
    
    func someAction(_ sender:UITapGestureRecognizer){
        for i in  0..<viewArray.count
        {
            if(tagArray[i] == sender.view?.tag && flag[i] == 0){
                viewArray[i].layer.cornerRadius = 2
                viewArray[i].layer.shadowColor = UIColor.black.cgColor
                viewArray[i].layer.shadowOffset = CGSize(width: 1, height: 1.0)
                viewArray[i].layer.shadowOpacity = 0.5
                viewArray[i].layer.shadowRadius = 5.0
                flag[i] = 1
            }
            else{
                removeShadowEffect(index: i)
                flag[i] = 0
            }
        }
    }
    
    //Mark: - Remove Shadow
    
    func removeShadowEffect(index:Int)
    {
        viewArray[index].layer.shadowOpacity = 0.0
    }
    
}

