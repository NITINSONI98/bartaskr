//
//  CollectionViewCell.swift
//  barChart
//
//  Created by Sierra 4 on 07/04/17.
//  Copyright © 2017 codebrew. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imgImage: UIImageView!
    @IBOutlet weak var lblCondition: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    
}
