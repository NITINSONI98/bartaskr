//
//  Constant.swift
//  barChart
//
//  Created by Sierra 4 on 10/04/17.
//  Copyright © 2017 codebrew. All rights reserved.
//

import Foundation
enum Identifier : String{
    case collectionIdentifier = "showDetails"
}
enum aryCondition : String{
    case DeepSleep = "Deep Sleep"
    case LightSleep = "Light Sleep"
    case Fellasleepat = "Fell asleep at"
    case Wokeupat = "Woke up at"
    case Awakefor = "Awake for"
    case inbedtime = "in bed time"
}
enum aryImage : String{
    case awake_for = "awake_for"
    case bed_time = "bed_time"
    case deepsleep = "deep sleep"
    case lightsleep = "light sleep"
    case woke_up_at = "woke_up_at"
    case feel_asleep_at = "feel_asleep_at"
}
enum aryTime : String{
    case one = "1h 25m"
    case two = "6h 02m"
    case three = "1:50 am"
    case four = "8:27 am"
    case five = "32m"
    case six = "8h 30m"
}
